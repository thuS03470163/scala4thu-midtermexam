import org.scalatest.FunSuite

class TailFacSpec extends FunSuite {
  test("tailFac(5,1)=120") {
    assert(tailFac(5, 1) == 120)

  }
  test("tailFac(7,1)=5040") {
    assert(tailFac(7, 1) == 5040)

  }
  test("tailFac(10,1)=3628800") {
    assert(tailFac(10, 1) == 3628800)

  }
  test("tailFac(10000000,1)") {
    assert(tailFac(10000000, 1) == 0)

  }
}
tailFac(8,1)


def tailFac(n:Int,acc:Int):Int= {
  if (n == 0) 1
  else n * tailFac(n - 1,acc:Int)
}


