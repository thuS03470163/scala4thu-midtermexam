import Questions.tailFac
import org.scalatest.FunSuite

/**
  * Created by mac014 on 2017/4/10.
  */
class one {

}
class TailFacSpec extends FunSuite{
  test("tailFac(5,1)=120") {
    assert(tailFac(5,1)==120)

  }
  test("tailFac(7,1)=5040") {
    assert(tailFac(7,1)==5040)

  }
  test("tailFac(10,1)=3628800") {
    assert(tailFac(10,1)==3628800)

  }
  test("tailFac(10000000,1)") {
    assert(tailFac(10000000,1)==0)

  }


}